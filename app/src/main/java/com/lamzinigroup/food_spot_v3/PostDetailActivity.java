package com.lamzinigroup.food_spot_v3;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.lamzinigroup.food_spot_v3.Extra.Comment;
import com.lamzinigroup.food_spot_v3.Extra.ImageViews.ImageListActivity;
import com.lamzinigroup.food_spot_v3.Extra.Post;
import com.lamzinigroup.food_spot_v3.Extra.User;
import com.lamzinigroup.food_spot_v3.component.ExpandableList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 18/03/2017.
 */

// The PostDetailActivity has been taken from Firebase Database Sample:
    // https://github.com/firebase/quickstart-android/tree/master/database

    // However, the author has made a number of changes as stated below

public class PostDetailActivity extends BaseActivity implements View.OnClickListener, OnMapReadyCallback {



    private static final String TAG = "PostDetailActivity";

    public static final String EXTRA_POST_KEY = "post_key";
    public static final String FB_DATABASE_PATH = "image";

    private DatabaseReference mPostReference;
    private DatabaseReference mCommentsReference;
    private ValueEventListener mPostListener;
    private String mPostKey;
    private CommentAdapter mAdapter;

    private TextView mAuthView;
    private TextView mTitleView;
    private TextView mBodyView;
    private EditText mCommentField;
    private Button mCommentButton;
    private RecyclerView mCommentsRecycler;

    // The below lines are the author code: (71 - 76)
    private Post newPost;
    private Post post;
    private GoogleMap map;
    private ExpandableList breakfast;
    private ExpandableList lunch;
    private ExpandableList dinner;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);

        // Get post key for the selected Firebase post
        mPostKey = getIntent().getStringExtra(EXTRA_POST_KEY);
        if (mPostKey == null){
            throw new IllegalArgumentException("Must pass EXTRA_POST_KEY");
        }

        // Initialise Database
        mPostReference = FirebaseDatabase.getInstance().getReference()
                .child("posts").child(mPostKey);
        mCommentsReference = FirebaseDatabase.getInstance().getReference()
                .child("post-comments").child(mPostKey);


        // Initialise views
        mAuthView = (TextView) findViewById(R.id.post_author);
        mTitleView = (TextView) findViewById(R.id.post_title);
        mBodyView = (TextView) findViewById(R.id.post_body);
        mCommentField = (EditText) findViewById(R.id.field_comment_text);
        mCommentButton = (Button) findViewById(R.id.button_post_comment);
        mCommentsRecycler = (RecyclerView) findViewById(R.id.recycler_comments);
        // the below lines of code is the author (105 - 107)
        breakfast = (ExpandableList) findViewById(R.id.breakfast);
        lunch = (ExpandableList) findViewById(R.id.lunch);
        dinner = (ExpandableList) findViewById(R.id.dinner);

        mCommentButton.setOnClickListener(this);
        mCommentsRecycler.setLayoutManager(new LinearLayoutManager(this));

        // the below two lines of code is the author (114) && (117)
        // google maps below initialising
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        // syncing the map view with the onMapReady by using this
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onStart(){
        super.onStart();

        // Add value event listener to the post
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get post object and use the values to update the user interface
                post = dataSnapshot.getValue(Post.class);
                newPost = post;
                //AuthView now contains title
                mAuthView.setText(post.title);
                // TitleView now contains address
                mTitleView.setText(post.address);
                mBodyView.setText(post.body);

                // the below is the author code (138 - 141)
                setMeal();
                if(map != null){
                    setLocation();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // If post failed, then log a message
                Log.w(TAG, "loadpost:onCancelled", databaseError.toException());

                Toast.makeText(PostDetailActivity.this , "Failed to load post.",
                        Toast.LENGTH_SHORT).show();
            }
        };
        // When the request is successful from Firebase, the below is what is return with the data
        // the below attaching the listener to the query created for "posts"
        mPostReference.addValueEventListener(postListener);

        // Keep a copy of the post listener so it can be removed when the app stops
        mPostListener = postListener;

        // Listen for comments
        mAdapter = new CommentAdapter(this, mCommentsReference);
        mCommentsRecycler.setAdapter(mAdapter);
    }

    // the onStop method is called when the activity stops (this called Activity life cycle event
    @Override
    public void onStop(){
        super.onStop();

        //Remove post value of the event listener
        if (mPostListener != null){
            mPostReference.removeEventListener(mPostListener);
        }

        // Clean up comments listener
        mAdapter.cleanupListener();
    }

    // the below method is called when the user presses the post button for the commenting
    @Override
    public void onClick(View v){
        int i = v.getId();
        if (i == R.id.button_post_comment){
            postComment();
        }
    }




    // The setMeal() code is the authors (196 - 221)
    // setMeal method deals with the menu items for a selected post
    // It first checks to see if there are any items in that type of menu
    // it then adds the item to an array
    // and displays the items to the user
    private void setMeal(){
        if(post.getBreakfast() != null){
            List<String> list1 = new ArrayList<>();
            list1.add(post.getBreakfast().getItem1());
            list1.add(post.getBreakfast().getItem2());
            breakfast.setUp("BreakFast", list1);
            breakfast.setVisibility(View.VISIBLE);
        }

        if(post.getLunch() != null){
            List<String> list1 = new ArrayList<>();
            list1.add(post.getLunch().getItem1());
            list1.add(post.getLunch().getItem2());
            lunch.setUp("Lunch", list1);
            lunch.setVisibility(View.VISIBLE);

        }

        if(post.getDinner() != null){
            List<String> list1 = new ArrayList<>();
            list1.add(post.getDinner().getItem1());
            list1.add(post.getDinner().getItem2());
            dinner.setUp("Dinner", list1);
            dinner.setVisibility(View.VISIBLE);
        }
    }


    private void postComment(){
        // get the Firebase id that is going to be used for that post
        final String uid = getUid();
        // go to the user section of Firebase to get the id
        FirebaseDatabase.getInstance().getReference().child("users").child(uid)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user information
                        User user = dataSnapshot.getValue(User.class);
                        String authorName = user.username;

                        // Create new comment object
                        String commentText = mCommentField.getText().toString();
                        com.lamzinigroup.food_spot_v3.Extra.Comment comment;
                        comment = new com.lamzinigroup.food_spot_v3.Extra.Comment(uid, authorName, commentText);

                        //Push the comment, it will appear in the list of comments
                        mCommentsReference.push().setValue(comment);

                        // Clear the filed for more comments
                        mCommentField.setText(null);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    // This is the on Map called used, which is called when the google map is ready (loaded up)
    // when loaded, this will set the post location on the map
    // The onMapReady and setLocation method code has been taken from Google Developer Guide (261 - 282):
        // https://google-developers.appspot.com/maps/documentation/android-api/maps-in-action_redesign
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if(post != null){
            setLocation();
        }
    }

    // this method adds the marker on the map
    void setLocation(){
        // adds the marker add the lon and lat pos
        map.addMarker(new MarkerOptions().position(new LatLng(post.getLat(), post.getLon())));
        // After the marker is added, the set the map to move that that location
        CameraPosition cameraPosition = new CameraPosition.Builder()
                //moves to the location
                .target(new LatLng(post.getLat(), post.getLon()))
                // zooms
                .zoom(15)
                // build it to see
                .build();
        // this creates a nice zoom panel (for additional effect)
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    // the below sets the comment views
    private static class CommentViewHolder extends RecyclerView.ViewHolder {

        public TextView authorView;
        public TextView bodyView;

        public CommentViewHolder(View itemView) {
            super(itemView);

            authorView = (TextView) itemView.findViewById(R.id.comment_author);
            bodyView = (TextView) itemView.findViewById(R.id.comment_body);
        }
    }

    // handles the way the commenting is done
    private static class CommentAdapter extends RecyclerView.Adapter<CommentViewHolder> {

        private Context mContext;
        private DatabaseReference mDatabaseReference;
        private ChildEventListener mChildEventListener;

        private List<String> mCommentIds = new ArrayList<>();
        private List<Comment> mComments = new ArrayList<>();

        public CommentAdapter(final Context context, DatabaseReference ref){
            mContext = context;
            mDatabaseReference = ref;

            //Create child event listener
            ChildEventListener childEventListener = new ChildEventListener() {
                @Override
                // the snapshot belows takes a snapshot of all the comments currently on firebase
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Log.d(TAG, "onChildAdded:" + dataSnapshot.getKey());


                    // Add the new comment to the displayed list
                    Comment comment = dataSnapshot.getValue(Comment.class);

                    // Update RecyclerView
                    mCommentIds.add(dataSnapshot.getKey());
                    mComments.add(comment);
                    notifyItemInserted(mComments.size() - 1);
                }

                @Override
                // the below method is called when commenting is edited, this is for an existing one
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    // A comment changed, use the unique key to see if it needs displaying for given post
                    Comment newComment = dataSnapshot.getValue(Comment.class);
                    String commentKey = dataSnapshot.getKey();

                    int commentIndex = mCommentIds.indexOf(commentKey);
                    if (commentIndex > -1) {
                        //replace with the new data / comment
                        mComments.set(commentIndex, newComment);

                        // Update the recyclerView
                        notifyItemChanged(commentIndex);
                    } else {
                        Log.w(TAG, "onChildChanged:unknown_child:" + commentKey);
                    }
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    Log.d(TAG, "onChildRemoved:" + dataSnapshot.getKey());

                    // A comment changed, use the unique key to see if it needs removing for the given post
                    String commentKey = dataSnapshot.getKey();

                    int commentIndex = mCommentIds.indexOf(commentKey);
                    if (commentIndex > -1) {
                        // Remove data from the list
                        mCommentIds.remove(commentIndex);
                        mComments.remove(commentIndex);

                        // Update the recyclerView
                        notifyItemRemoved(commentIndex);
                    } else {
                        Log.w(TAG, "onChildRemoved:unknown_child:" + commentKey);
                    }

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    Log.d(TAG, "onChildMoved:" + dataSnapshot.getKey());

                    // A comment changed, use the unique key to see if it needs moving
                    Comment movedComment = dataSnapshot.getValue(Comment.class);
                    String commentKey = dataSnapshot.getKey();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.w(TAG, "postComments:onCancelled", databaseError.toException());
                    Toast.makeText(mContext, "Failed to load comments",
                            Toast.LENGTH_SHORT).show();
                }
            };
            ref.addChildEventListener(childEventListener);

            //Store reference to listener so it can be removed when the app stops running
            mChildEventListener = childEventListener;
        }

        @Override
        // this links up the view
        public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            View view = inflater.inflate(R.layout.item_comment, parent, false);
            return new CommentViewHolder(view);
        }

        @Override
        // this deals with displaying the data at the current position
        public void onBindViewHolder(CommentViewHolder holder, int position) {
            Comment comment = mComments.get(position);
            holder.authorView.setText(comment.author);
            holder.bodyView.setText(comment.text);
        }

        @Override
        // the amount of comments made
        public int getItemCount(){
            return mComments.size();
        }

        public void cleanupListener (){
            if (mChildEventListener != null){
                mDatabaseReference.removeEventListener(mChildEventListener);
            }
        }
    }

    // the below method is the author code (423 - 428)
    // this method is called when the user presses the Show Images button
    public void btnShowListImage_Click(View v){
        Intent i = new Intent (PostDetailActivity.this, ImageListActivity.class);
        i.putExtra("imageUrl", newPost.getImageURL());
        i.putExtra("imageUrl2", newPost.getImageURL2());
        i.putExtra("imageUrl3", newPost.getImageURL3());
        startActivity(i);

    }
}

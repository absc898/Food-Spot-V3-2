package com.lamzinigroup.food_spot_v3;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.lamzinigroup.food_spot_v3.util.TemplateCallback;

import java.util.ArrayList;

// the TabbedActivity code is auto generated code from Android Studio and the author has made small changes stated in the comments below
// tabbedActivity is the parent / the order of the two fragments, which mainfragement and templatefragment
public class TabbedActivity extends AppCompatActivity implements TemplateCallback{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private TabLayout tabLayout;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabbed);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // setting the action to the toolbar (taking control)
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        // the below code handles the control of the fragments used
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        // ViewPager handles where the adapter is going to be used
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        // connects the two tab layout to the view pager (when you swipe to the next page, it keeps it in sync, go to the next fragment)
        tabLayout.setupWithViewPager(mViewPager);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dots_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(this, MainActivity.class));
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(String text) {
        // This handle the on click for template only.
        // When template is selected, it goes back to the main fragment and it passes the text, which is template back to the section
        // pager adapter as a query
        tabLayout.getTabAt(0).select();
        mSectionsPagerAdapter.templateQuery(text);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
            // this method is in charge of the creation of fragments
        ArrayList<Fragment> fragments = new ArrayList<>();
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            // the below two lines of code is the author (109 - 110)
            fragments.add(new MainFragment());
            fragments.add(new TemplateFragment());
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            // this returns the fragment position
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                // depending on which position the user is currently, the following case will be used
                case 0:
                    return "Search";
                case 1:
                    return "Template";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }

        // the below method is the author code ( 143 - 145)
        // this calls the fetchpost method in mainFragment for the search in both search fragement and template fragment
        public void templateQuery(String text){
            ((MainFragment)fragments.get(0)).fetchPost(text);
        }
    }
}

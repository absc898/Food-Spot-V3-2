package com.lamzinigroup.food_spot_v3;

/**
 * Created by abdullamzini on 29/05/2017.
 */

// The below class is the author code (8 - 33)
public class Meal {

    private String item1;
    private String item2;

    public Meal() {

    }

    public String getItem1() {

        return item1;
    }

    public void setItem1(String item1) {
        this.item1 = item1;
    }

    public String getItem2() {
        return item2;
    }

    public void setItem2(String item2) {
        this.item2 = item2;
    }
}

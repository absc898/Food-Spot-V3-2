package com.lamzinigroup.food_spot_v3;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.lamzinigroup.food_spot_v3.Extra.Post;
import com.lamzinigroup.food_spot_v3.util.LocationCallback;
import com.lamzinigroup.food_spot_v3.util.LocationDetector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS;


// The MainFragment class has been taken from the Firebase Database Sample:
    //https://github.com/firebase/quickstart-android/tree/master/database
    // However, a number of different features have been added. The author has stated below, which is their code.
public class MainFragment extends Fragment implements LocationCallback {
    private static final String TAG = "Main2Activity";

    private EditText searchEditText;
    LocationDetector locationDetector;
    private RecyclerView mRecycler;
    PostAdapter postAdapter;
    // hashmap is a data structure for storing items in map form. The item pass is the key the used
    // the line below is the author code (60)
    private HashMap<String, GeoLocation> restaurantLocations = new HashMap<>();


    @Nullable
    @Override
    // As a fragment is also a view. its needs to be inflated, which turns the XML into a view
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_main2,container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecycler = (RecyclerView) view.findViewById(R.id.messages_list);
        mRecycler.setHasFixedSize(true);
        postAdapter = new PostAdapter(getContext(), getUid());
        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecycler.setAdapter(postAdapter);
        // check to see if the permission for location is given
        // the below line of code is the author (82)
        checkPermission();

        // gets the text of the search bar and calls the fetchPost on that text
        // the below lines of code are the author code (86 - 92)
        searchEditText = (EditText) view.findViewById(R.id.searchView);
        view.findViewById(R.id.search).setOnClickListener(new View.OnClickListener() {
            @Override
            // this gets called when the user clicks on the search button
            public void onClick(View view) {
                fetchPost(searchEditText.getText().toString());
            }
        });

    }

    private void checkPermission() {
        // The below code has been taken from Google Runtime Permission (Google APIs for Android):
            // https://developers.google.com/android/guides/permissions
        // This checks the permission for the location by using a popup
        if (ActivityCompat.checkSelfPermission(getContext(), ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
                    900);
            // the 900 above is the request code
            return;
        }
        // this calls the location detector to get the user location
        // The below lines of code is the author code (111 - 112 && 114)
        locationDetector = new LocationDetector(getContext(), this);
        locationDetector.connect();
        // see if the device location service is on
        checkLocationStatus();
    }

    @Override
    // The below checks the permission has been granted when the user presses yes
    // this is called when the user comes back into the app to see if they pressed yes (after location request)
    // The below method has been taken from the Android Developer Guide:
        // https://developer.android.com/reference/android/support/v4/app/ActivityCompat.OnRequestPermissionsResultCallback.html
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 900: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // the below lines of code are the author (125 - 127)
                    locationDetector = new LocationDetector(getContext(), this);
                    locationDetector.connect();
                    checkLocationStatus();
                }
            }
        }
    }



    private void checkLocationStatus(){
        // turns on the location service on the actual device
        // The below lines of code has been taken from Stack Over Flow (141 - 165):
            // https://stackoverflow.com/questions/7673123/how-to-check-if-locationmanager-network-provider-is-available
        LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        // check to see if the location service is on. if not, the user gets a pop dialog to turn it on
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Location")
                    .setMessage("Please enable your Location Service")
                    .setCancelable(false)
                    .setPositiveButton("Turn on", new DialogInterface.OnClickListener() {
                        @Override
                        // if the user click on turn on, it will take them to their location setting
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivity(new Intent(ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    })
                    // if they press cancel, the dialog will disappear
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
            // builds the dialog and then display it
            builder.create().show();
        }
    }

    @Override
    // This method has been taken from Firebase Database Sample:
        // https://github.com/firebase/quickstart-android/tree/master/database
    public boolean onOptionsItemSelected(MenuItem item){
        int i = item.getItemId();
        if (i == R.id.action_logout) {
            FirebaseAuth.getInstance().signOut();
            // the below line of code is the author (175)
            startActivity(new Intent(getContext(), MainActivity.class));
            getActivity().finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    // this is a callback when the user location have been added.
    // this calls the get post method passing the user lat and long
    public void onLocationDetected(double lat, double lon) {
        // the below two lines of code is the author code (188 - 189)
        Log.d("location", "lat"+lat+" lon"+lon);
        getPost(lat, lon);
    }

    private void getPost(double lat, double lon) {
        // this makes a request to Firebase to retrieve all the restaurants within the user location
        // the below two lines of code is the author code (195 - 196)
        Log.d("location", "lat"+lat+" lon"+lon);
        DatabaseReference dataRef = FirebaseDatabase.getInstance().getReference("restuarant-location");
        // The following lines (199 - 235) for code has been taken from GeoFire library:
            // https://github.com/firebase/geofire-java
        final GeoFire geoFire = new GeoFire(dataRef);
        // the blow line sets the radius of the search using the geo fire library
        // searches for restaurants within 50 kilometers of the user proximity
        GeoQuery query = geoFire.queryAtLocation(new GeoLocation(lat,lon), 50);
        query.addGeoQueryEventListener(new GeoQueryEventListener() {
            // getting the restaurant location using Geofire
            // the key is every location of each restaurant
            // saving the location returned in a hash map
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                // this is a callback for each item at the location
                // then its gets added to resturantLocations hash map
                // this uses the GeoFire library, which has already done the comparison
                restaurantLocations.put(key, location);
            }

            // this remove the location, however this is not needed as the restaurants do not move
            @Override
            public void onKeyExited(String key) {
                restaurantLocations.remove(key);
            }
            // this is used when a location has changed within the radius
            @Override
            public void onKeyMoved(String key, GeoLocation location) {

            }
            // when the location is loaded in, fetchPost is then called to display those items to the user
            @Override
            public void onGeoQueryReady() {
                fetchPost(null);
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {

            }
        });

    }


    // The fetchPost method is the author code ( 241 - 306)
    public void fetchPost(String type){
        // for MainFragment, the search button calls this method
        // for TemplateFragment, the image views call this method
        // first get every posts in Firebase and put it in an array
        // This is going to be used for the comparision of Geofire
        final List<Post> posts = new ArrayList<>();
        // this is called when a query is made on a node on Firebase
        // this will return everything for a searched node
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // clear the post list so that there are no duplication
                posts.clear();
                // the below line converts the datasnapshot from Firebase into the java base hashmap that can be used
                GenericTypeIndicator<HashMap<String, Post>> typeIndicator = new GenericTypeIndicator<HashMap<String, Post>>(){};
                // a for loop is used to see if the resturant is inside the user location, if so, it gets added to posts array list
                for(Post post:new ArrayList<Post>(dataSnapshot.getValue(typeIndicator).values())){
                    if(restaurantLocations.containsKey(post.uid)){
                        posts.add(post);
                    }
                }

                postAdapter.setPost(posts);
            }

            // The below line of code is auto generated (267 - 270)
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        if(type == null) {
            FirebaseDatabase.getInstance().getReference("posts").addValueEventListener(listener);
        }
        else if(type.equals("")){
            FirebaseDatabase.getInstance().getReference("posts").addValueEventListener(listener);
        }
        else if(type.equals("rooftop")){
            FirebaseDatabase.getInstance().getReference("posts").orderByChild("rooftop").equalTo("on").addValueEventListener(listener);
        }
        else if(type.equals("scenic view")){
            FirebaseDatabase.getInstance().getReference("posts").orderByChild("scenicView").equalTo("on").addValueEventListener(listener);
        }
        else if(type.equals("kids")){
            FirebaseDatabase.getInstance().getReference("posts").orderByChild("kidsArea").equalTo("on").addValueEventListener(listener);
        }
        else if(type.equals("shisha")){
            FirebaseDatabase.getInstance().getReference("posts").orderByChild("shishaBar").equalTo("on").addValueEventListener(listener);
        }
        else if(type.equals("group bookings")){
            FirebaseDatabase.getInstance().getReference("posts").orderByChild("groupBooking").equalTo("on").addValueEventListener(listener);
        }
        else if(type.equals("ocean view")){
            FirebaseDatabase.getInstance().getReference("posts").orderByChild("oceanView").equalTo("on").addValueEventListener(listener);
        }
        else if(type.equals("sky view")){
            FirebaseDatabase.getInstance().getReference("posts").orderByChild("skyView").equalTo("on").addValueEventListener(listener);
        }
        else if(type.equals("outside area")){
            FirebaseDatabase.getInstance().getReference("posts").orderByChild("outsideArea").equalTo("on").addValueEventListener(listener);
        }
        else if(type.equals("pool")){
            FirebaseDatabase.getInstance().getReference("posts").orderByChild("poolBar").equalTo("on").addValueEventListener(listener);
        }

    }





        // gets the user id and returns it
    // The below line has been taken from Firebase Database Sample:
        // https://github.com/firebase/quickstart-android/tree/master/database
    public String getUid(){
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }
}

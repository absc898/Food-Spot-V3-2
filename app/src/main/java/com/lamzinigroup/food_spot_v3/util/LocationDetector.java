package com.lamzinigroup.food_spot_v3.util;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by apple on 14/04/2017.
 */

public class LocationDetector implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener{

    private GoogleApiClient googleApiClient;
    private Context context;
    private LocationCallback locationCallback;
    private boolean connected;

    public LocationDetector(Context context, LocationCallback locationCallback) {
        this.context = context;
        this.locationCallback = locationCallback;
    }

    @Override
    public void onLocationChanged(Location location) {
        // this gets called every time a new location is obtained
        // this calls the location call back and gives it the users location
        // this uses the Location class to get the users location
        locationCallback.onLocationDetected(location.getLatitude(), location.getLongitude());
        Log.d("location", "lat"+location.getLatitude()+" lon"+location.getLongitude());
    }

    public void connect() {
        // This connect to google api
        googleApiClient = new GoogleApiClient.Builder(context).addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        googleApiClient.connect();
    }

    public void disconnect() {
        // this disconnect from the google api to help reduce bandwidth / speed of it constantly being connnected
        if (connected) {
            googleApiClient.disconnect();
            connected = false;
        }
    }

    @Override
    // when the api is connected, the below request for the user location
    public void onConnected(@Nullable Bundle bundle) {
        // This get the user permission first to use their location
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationRequest locationRequest = LocationRequest.create();
            // this request to make accurate judgement of the location
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            // this keeps getting their location every 60 seconds
            locationRequest.setInterval(60000);
            // if anything goes wrong, instead crashing the application, it catches the error
            // and then tell the api client to connect again
            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient,
                        locationRequest, this);
            }
            catch (Exception e){
                googleApiClient.connect();
            }

            connected = true;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {


    }
}

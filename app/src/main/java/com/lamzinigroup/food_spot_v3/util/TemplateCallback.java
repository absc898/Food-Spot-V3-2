package com.lamzinigroup.food_spot_v3.util;

/**
 * Created by abdullamzini on 17/06/2017.
 */

public interface TemplateCallback {
    // this is used for the tabbed activity for when a template is clicked
    void onClick(String text);
}

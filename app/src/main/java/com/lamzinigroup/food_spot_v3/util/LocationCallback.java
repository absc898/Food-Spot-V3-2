package com.lamzinigroup.food_spot_v3.util;

/**
 * Created by apple on 14/04/2017.
 */

public interface LocationCallback {
    // this is an interface which is a call, which gives a different implementation for this method
    // this allows this method to be used as a call back so that other methods can make use of this
    void onLocationDetected(double lat, double lon);
}

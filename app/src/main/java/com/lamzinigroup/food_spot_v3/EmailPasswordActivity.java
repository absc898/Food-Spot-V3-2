package com.lamzinigroup.food_spot_v3;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by apple on 30/01/2017.
 */

// The EmailPasswordActivity has been taken from the Firebase Auth Sample:
    //https://github.com/firebase/quickstart-android/tree/master/auth

public class EmailPasswordActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "EmailPassword";

    private TextView mStatusTextView;
    private TextView mDetailTextView;
    private EditText mEmailField;
    private EditText mPasswordField;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emailpassword);

        // linking up views
        mStatusTextView = (TextView) findViewById(R.id.status);
        mDetailTextView = (TextView) findViewById(R.id.detail);
        mEmailField = (EditText) findViewById(R.id.field_email);
        mPasswordField = (EditText) findViewById(R.id.field_password);

        // linking up buttons
        findViewById(R.id.email_sign_in_button).setOnClickListener(this);
        findViewById(R.id.email_create_account_button).setOnClickListener(this);
        findViewById(R.id.sign_out_button).setOnClickListener(this);

        // the below gets a shared instance of the FirebaseAuth object
        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user!= null){
                    // User is logged in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    // Should go to a new screen
                    // startActivity(new Intent(this, MainActivity.class));
                } else {
                    //User is logged out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // update the interface to corresponds if there logged in or not
                updateUI(user);
            }
        };

    }
    // the on start method is an activity life cycle
    // in this case the activity starts
    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    // the activity is about to stop
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private void createAccount(String email, String password) {
        Log.d(TAG, "createAccount:" + email);
        if (!validateForm()) {
            return;
        }

        showProgressDialog();

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                Log.d(TAG, "CreateUserWithEmail:onComplete:" + task.isSuccessful());

                // if sign in fail then display message
                // if sign in succeeds then the auth state listener will trigger

                if (task.isSuccessful()) {
                   // this will begin the next activity (go to the next screen, which is Main2Activity)
                    startActivity(new Intent(EmailPasswordActivity.this, TabbedActivity.class));
                } else if (!task.isSuccessful()) {
                    Log.w(TAG, "signInWithEmail:failed", task.getException());
                    // display an error message to the user
                    // The below line is the author code (113 - 114)
                    Toast.makeText(EmailPasswordActivity.this, "Authentication has failed, please check you have used a valid email address " +
                            "and please ensure the password is 6 or more characters", Toast.LENGTH_LONG).show();
                }

                hideProgressDialog();
            }
        });
    }

    private void signIn (String email, String password){
        Log.d(TAG, "signIn:" + email);
        if (!validateForm()) {
            return;
        }

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());

                if (task.isSuccessful()) {
                    // This will begin the next activity (go to the next screen, which is TabbedActivity)
                    // The below line is the author code (137)
                    startActivity(new Intent(EmailPasswordActivity.this, TabbedActivity.class));

                } else if (!task.isSuccessful()) {
                    // The user has failed to log, first will display an error with the console for further debugging
                    Log.w(TAG, "signInWithEmail:failed", task.getException());
                    // Toast will display an error message to the user
                    // The line below is the author code (144 - 145)
                    Toast.makeText(EmailPasswordActivity.this, "Authentication has failed, please check you have used a valid email " +
                            "address and please ensure the password is 6 or more characters", Toast.LENGTH_LONG).show();
                }

                hideProgressDialog();
            }
        });
    }
    // this sign out the user
    private void signOut(){
        mAuth.signOut();
        updateUI(null);
    }
    // this function provides validation on the email and password field
    // this checks to see if the field are empty or not
    // if empty an error is displayed
    private boolean validateForm(){
        boolean valid = true;

        String email = mEmailField.getText().toString();
        if (TextUtils.isEmpty(email)){
            mEmailField.setError("Required");
            valid = false;
        } else {
            mEmailField.setError(null);
        }

        String password = mPasswordField.getText().toString();
        if (TextUtils.isEmpty(password)){
            mPasswordField.setError("Required - Must be 6 or more letters");
            valid = false;
        } else {
            mPasswordField.setError(null);
        }

        return valid;
    }
    // UpdateUI handles updating the design view with details with
    // whether the user is logged in or not
    private void updateUI(FirebaseUser user){
        hideProgressDialog();
        if (user != null) {
            mStatusTextView.setText(getString(R.string.emailpassword_status_fmt, user.getEmail()));
            mDetailTextView.setText(getString(R.string.firebase_status_fmt, user.getUid()));

            //findViewById(R.id.email_password_buttons)
            //findViewById(R.id.email_password_fields)
            findViewById(R.id.sign_out_button).setVisibility(View.VISIBLE);
        } else {
            mStatusTextView.setText("Signed Out");
            mDetailTextView.setText(null);

            //findViewById(R.id.email_password_buttons)
            //findViewById(R.id.email_password_fields)
            findViewById(R.id.sign_out_button).setVisibility(View.VISIBLE);

        }
    }

    @Override
    // this checks to see if the create account, sign in or sign out is pressed
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.email_create_account_button) {
            createAccount(mEmailField.getText().toString(), mPasswordField.getText().toString());
        } else if (i == R.id.email_sign_in_button) {
            signIn(mEmailField.getText().toString(), mPasswordField.getText().toString());
        } else if (i == R.id.sign_out_button) {
            signOut();
        }
    }

}
package com.lamzinigroup.food_spot_v3.Extra.ImageViews;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.StorageReference;
import com.lamzinigroup.food_spot_v3.PostDetailActivity;
import com.lamzinigroup.food_spot_v3.R;

import java.util.ArrayList;
import java.util.Arrays;

// The ImageListActivity is the author code
public class ImageListActivity extends AppCompatActivity implements ImageListAdapter.ImageLoad{

    private DatabaseReference mDatabaseRef;
    private ListView lv;
    private ImageListAdapter adapter;
    private ProgressDialog progressDialog;
    private StorageReference mStorageRef;
    private ImageView imageView;
    private String imageUrl;
    private String imageUrl2;
    private String imageUrl3;
    private RecyclerView recyclerView;



    // Lines 38 - 40 are auto generated code
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_list);

        // passing the image URL from the Adapter image class
        imageUrl = getIntent().getStringExtra("imageUrl");
        imageUrl2 = getIntent().getStringExtra("imageUrl2");
        imageUrl3 = getIntent().getStringExtra("imageUrl3");

        // storing the URL's into the adapter
        ArrayList<String> images = new ArrayList<>(Arrays.asList(imageUrl,imageUrl2,imageUrl3
        ));

        recyclerView = (RecyclerView)findViewById(R.id.imageList);


        adapter = new ImageListAdapter(this,this);
        // setting the images into the adapter
        adapter.setImages(images);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait, loading images...");
        progressDialog.show();


        mDatabaseRef = FirebaseDatabase.getInstance().getReference(PostDetailActivity.FB_DATABASE_PATH);

    }

    @Override
    public void onLoaded() {
        progressDialog.dismiss();
    }
}

package com.lamzinigroup.food_spot_v3.Extra;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.lamzinigroup.food_spot_v3.Meal;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by apple on 05/02/2017.
 */

// This class has been taken from Firebase Database Sample:
    //https://github.com/firebase/quickstart-android/tree/master/database

    //However, the author has added a number of lines of code as shown in the comments below.

// This is a mapper class which maps all the data from firebase to the android application
@IgnoreExtraProperties
public class Post {


    public String uid;
    public String author;
    public String title;
    public String body;
    public int starCount = 0;
    public Map<String, Boolean> stars = new HashMap<>();

    // Lines 34 - 51 are the authors code
    public String address;
    public String imageURL;
    public String imageURL2;
    public String imageURL3;
    public Meal breakfast;
    public Meal lunch;
    public Meal dinner;
    public double lat;
    public double lon;
    public String rooftop;
    public String scenicView;
    public String kidsArea;
    public String shishaBar;
    public String groupBooking;
    public String oceanView;
    public String skyView;
    public String outsideArea;
    public String poolBar;



    public Post(String uid, String author, String title, String body, String address,
                String imageURL,String imageURL2, String imageURL3, List<String> breakfast ,
                List<String> lunch, List<String> dinner, String roofop, String scenicView,
                String kidsArea, String shishaBar, String groupBooking, String oceanView, String skyView, String outsideArea, String poolBar) {
        this.uid = uid;
        this.author = author;
        this.title = title;
        this.body = body;
        // Lines 64 - 76 are the the authors code
        this.address = address;
        this.imageURL = imageURL;
        this.imageURL2 = imageURL2;
        this.imageURL3 = imageURL3;
        this.rooftop = roofop;
        this.scenicView = scenicView;
        this.kidsArea = kidsArea;
        this.shishaBar = shishaBar;
        this.groupBooking = groupBooking;
        this.oceanView = oceanView;
        this.skyView = skyView;
        this.outsideArea = outsideArea;
        this.poolBar = poolBar;
    }

    // Lines 80 -  268 are the authors code
    public String getKidsArea() {
        return kidsArea;
    }

    public void setKidsArea(String kidsArea) {
        this.kidsArea = kidsArea;
    }

    public String getShishaBar() {
        return shishaBar;
    }

    public void setShishaBar(String shishaBar) {
        this.shishaBar = shishaBar;
    }

    public String getGroupBooking() {
        return groupBooking;
    }

    public void setGroupBooking(String groupBooking) {
        this.groupBooking = groupBooking;
    }

    public String getOceanView() {
        return oceanView;
    }

    public void setOceanView(String oceanView) {
        this.oceanView = oceanView;
    }

    public String getSkyView() {
        return skyView;
    }

    public void setSkyView(String skyView) {
        this.skyView = skyView;
    }

    public String getOutsideArea() {
        return outsideArea;
    }

    public void setOutsideArea(String outsideArea) {
        this.outsideArea = outsideArea;
    }

    public String getPoolBar() {
        return poolBar;
    }

    public void setPoolBar(String poolBar) {
        this.poolBar = poolBar;
    }

    public String getRooftop() {
        return rooftop;
    }

    public void setRooftop(String rooftop) {
        this.rooftop = rooftop;
    }

    public String getScenicView() {
        return scenicView;
    }

    public void setScenicView(String scenicView) {
        this.scenicView = scenicView;
    }

    public String getImageURL2() {
        return imageURL2;
    }

    public void setImageURL2(String imageURL2) {
        this.imageURL2 = imageURL2;
    }

    public String getImageURL3() {
        return imageURL3;
    }

    public void setImageURL3(String imageURL3) {
        this.imageURL3 = imageURL3;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getStarCount() {
        return starCount;
    }

    public void setStarCount(int starCount) {
        this.starCount = starCount;
    }

    public Map<String, Boolean> getStars() {
        return stars;
    }

    public void setStars(Map<String, Boolean> stars) {
        this.stars = stars;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public Meal getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(Meal breakfast) {
        this.breakfast = breakfast;
    }

    public Meal getLunch() {
        return lunch;
    }

    public void setLunch(Meal lunch) {
        this.lunch = lunch;
    }

    public Meal getDinner() {
        return dinner;
    }

    public void setDinner(Meal dinner) {
        this.dinner = dinner;
    }

    public Post(){

    }

    @Exclude
    public Map <String, Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", uid);
        result.put("author", author);
        result.put("title", title);
        result.put("body", body);
        result.put("starCount", starCount);
        result.put("stars", stars);
        // the below lines are the authors code (286 - 293)
        result.put("address", address);
        result.put("imageURL", imageURL);
        result.put("imageURL", imageURL2);
        result.put("imageURL", imageURL3);
        result.put("lat", lat);
        result.put("lon", lon);
        result.put("rooftop", rooftop);
        result.put("scenicView", scenicView);


        return result;

    }
}

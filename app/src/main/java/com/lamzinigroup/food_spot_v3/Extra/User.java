package com.lamzinigroup.food_spot_v3.Extra;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by apple on 18/03/2017.
 */

// This class has been taken from Firebase Database Sample:
    // https://github.com/firebase/quickstart-android/tree/master/database
@IgnoreExtraProperties
public class User {

    public String username;
    public String email;

    public User(){
        // Default constructor
    }

    public User(String username, String email){
        this.username = username;
        this.email = email;
    }
}



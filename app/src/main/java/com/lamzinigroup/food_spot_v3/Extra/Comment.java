package com.lamzinigroup.food_spot_v3.Extra;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by apple on 18/03/2017.
 */

// The class has been taken from the Firebase Database Sample:
    //https://github.com/firebase/quickstart-android/tree/master/database
@IgnoreExtraProperties
public class Comment {

    public String uid;
    public String author;
    public String text;

    public Comment(){
        //default constructor
    }

    public Comment(String uid, String author, String text){
        this.uid = uid;
        this.author = author;
        this.text = text;
    }
}

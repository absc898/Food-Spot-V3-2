package com.lamzinigroup.food_spot_v3.Extra.ImageViews;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.lamzinigroup.food_spot_v3.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by apple on 20/03/2017.
 */

// The ImageListAdapter is the author code
    // the author has also used Picasso library as well as stated in the comments
public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ViewHolder> {
    private List<String> images;
    // context here store information about the ImageListActivity
    private Context context;
    private ImageLoad imageLoad;

    public ImageListAdapter(Context context, ImageLoad imageLoad) {
        this.context = context;
        this.imageLoad = imageLoad;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // get the URL position (either 1, 2 or 3)
        String url = images.get(position);
        // using the picasso library, this gets images from the URL and loads into an image view
        // The below line of code is from the picasso library:
            // http://square.github.io/picasso/
        Picasso.with(context).load(url).into(holder.imageView, new Callback() {
            @Override
            public void onSuccess() {
                imageLoad.onLoaded();
            }

            @Override
            public void onError() {

            }
        });
        //notifyDataSetChanged();
    }

    @Override
    // the amount of the selected post
    // this is used by the framework (adapter)
    // this tells the onBindViewHolder how many times it needs to be called
    public int getItemCount() {
        return images.size();
    }

    void setImages(List<String> images){
        // this sets the image after images have been loaded from Firebase
        this.images = images;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        // the below displays the image
        private ImageView imageView;
        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView)itemView.findViewById(R.id.image);
        }
    }

    public interface ImageLoad{
        void onLoaded();
    }
}

package com.lamzinigroup.food_spot_v3;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.lamzinigroup.food_spot_v3.Extra.Post;

import java.util.List;

import static com.lamzinigroup.food_spot_v3.R.id.address;

/**
 * Created by apple on 20/03/2017.
 */


// The below classes have been take from Firebase Database Sample:
    // https://github.com/firebase/quickstart-android/tree/master/database

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostViewHolder> {
    private List<Post> postList;
    private Context context;
    private String uid;
    private DatabaseReference mDatabase;

    public PostAdapter(Context context, String uid) {
        this.context = context;
        this.uid = uid;
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    // turns every restaurant post into a view
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post, parent, false);
        return new PostViewHolder(view);
    }

    @Override
    // onBindViewHolder is in charge of how to display the view
    public void onBindViewHolder(final PostViewHolder holder, int position) {
        // this is going to be called for each item the user clicks to display the next screen for more information
        final Post post = postList.get(position);
        holder.bindToPost(post, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Need to write to both places the post is stored
                DatabaseReference globalPostRef = mDatabase.child("posts").child(post.getUid());
                DatabaseReference userPostRef = mDatabase.child("user-posts").child(uid).child(post.getUid());

                // Run two transactions
                onStarClicked(holder.numStarsView, globalPostRef);
                onStarClicked(holder.numStarsView, userPostRef);
            }
        });
    }

    // the amount of items that is going to be displayed on the first screen
    @Override
    public int getItemCount() {
        if(postList == null) return 0;
        return postList.size();
    }

    void setPost(List<Post> post) {
        postList = post;
        // tells the adapter to refresh it self if any more posts gets added
        notifyDataSetChanged();
    }

    // the below class is the holder for what the user see for each post before clicking on it
    public class PostViewHolder extends RecyclerView.ViewHolder {


        public TextView titleView;
        public TextView authorView;
        public ImageView starView;
        public TextView numStarsView;
        public TextView bodyView;
        public TextView addressView;
        public ImageView resView;


        public PostViewHolder(View itemView) {
            super(itemView);
            // title view is now description
            titleView = (TextView) itemView.findViewById(R.id.post_title);
            // authorView is now having the title
            authorView = (TextView) itemView.findViewById(R.id.post_author);
            starView = (ImageView) itemView.findViewById(R.id.star);
            numStarsView = (TextView) itemView.findViewById(R.id.post_num_stars);
            bodyView = (TextView) itemView.findViewById(R.id.post_body);
            addressView = (TextView) itemView.findViewById(address);
          //  resView = (ImageView) itemView.findViewById(R.id.resImage);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // this gets the position number and calls PostDetailActivity on that position
                    Intent intent = new Intent(context,PostDetailActivity.class);
                    // the post_key is used to say what information should get displayed
                    intent.putExtra("post_key", postList.get(getAdapterPosition()).getUid());
                    context.startActivity(intent);
                }
            });
        }


        // this sets the data from Firebase to the view, which came from onBindViewHolder.
        public void bindToPost(Post post, View.OnClickListener starClickListener) {
            titleView.setText("Description:");
            authorView.setText(post.title);
            numStarsView.setText(String.valueOf(post.starCount));
            bodyView.setText(post.body);
            // The below line is the author code (124)
            addressView.setText("Address: " + post.address);

            if (post.stars.containsKey(uid)) {
                starView.setImageResource(R.drawable.ic_toggle_star_24);
            } else {
                starView.setImageResource(R.drawable.ic_toggle_star_outline_24);
            }


            starView.setOnClickListener(starClickListener);
        }
    }

    public void onStarClicked (final TextView textView, DatabaseReference postRef){

        postRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Post starsP = mutableData.getValue(Post.class);
                if (starsP == null){
                    return Transaction.success(mutableData);
                }

                if (starsP.stars.containsKey(uid)){
                    // This will remove a star for this user id
                    //textView.setText(starsP.starCount - 1);
                    starsP.starCount = starsP.starCount - 1;
                    starsP.stars.remove(uid);
                } else {
                    // This will add a star for this user id
                   // textView.setText(starsP.starCount + 1);
                    starsP.starCount = starsP.starCount + 1;
                    starsP.stars.put(uid, true);
                }

                // set the value and the transcation to sucessful
                mutableData.setValue(starsP);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                textView.setText(String.valueOf(dataSnapshot.getValue(Post.class).starCount));
            }
        });

    }


}

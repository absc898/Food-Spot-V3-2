package com.lamzinigroup.food_spot_v3;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;


public class TemplateFragment extends Fragment implements OnClickListener{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.template_view,container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // All the code below is the author code ( 27 - 73)

        // linking up the images to an on click listener
        view.findViewById(R.id.rooftopButton).setOnClickListener(this);
        view.findViewById(R.id.scenicButton).setOnClickListener(this);
        view.findViewById(R.id.kidsButton).setOnClickListener(this);
        view.findViewById(R.id.shishaButton).setOnClickListener(this);
        view.findViewById(R.id.groupButton).setOnClickListener(this);
        view.findViewById(R.id.oceanButton).setOnClickListener(this);
        view.findViewById(R.id.skyButton).setOnClickListener(this);
        view.findViewById(R.id.outsideButton).setOnClickListener(this);
        view.findViewById(R.id.poolButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rooftopButton:
                getTabbedActivity().onClick("rooftop");
                break;
            case R.id.scenicButton:
                getTabbedActivity().onClick("scenic view");
                break;
            case R.id.kidsButton:
                getTabbedActivity().onClick("kids");
                break;
            case R.id.shishaButton:
                getTabbedActivity().onClick("shisha");
                break;
            case R.id.groupButton:
                getTabbedActivity().onClick("group bookings");
                break;
            case R.id.oceanButton:
                getTabbedActivity().onClick("ocean view");
                break;
            case R.id.skyButton:
                getTabbedActivity().onClick("sky view");
                break;
            case R.id.outsideButton:
                getTabbedActivity().onClick("outside area");
                break;
            case R.id.poolButton:
                getTabbedActivity().onClick("pool");
                break;
        }
    }

    // This returns the parent activity, which is tabbed activity
    // this call the on click method in tabbedActivity in order to filter by the keyword search
    private TabbedActivity getTabbedActivity(){
        return (TabbedActivity) getActivity();
    }
}

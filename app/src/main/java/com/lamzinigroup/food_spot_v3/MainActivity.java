package com.lamzinigroup.food_spot_v3;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

// The MainActivity has been taken from the Firebase Auth Sample:
    //https://github.com/firebase/quickstart-android/tree/master/auth
public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private static final Class[] CLASSES = new Class [] {

            EmailPasswordActivity.class,
            GoogleSignInActivity.class

    };

    private static final int[] DESCRIPTION_IDS = new int[]{

            R.string.desc_emailpassword,
           // R.id.EmailandPasswordIcon,
           // R.id.imageDocIcon2,
            R.string.desc_google_sign_in
    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setup ListView and Adapter

        ListView listView = (ListView) findViewById(R.id.list_view);
        // sets the adapter to use the sign_in_list XML view
        MyArrayAdapter adapter = new MyArrayAdapter(this, R.layout.sign_in_list, CLASSES);
        adapter.setDescriptionIds(DESCRIPTION_IDS);
        // sets the adapter and also sets a on click listener to view  to see which login option has been selected
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // checks to see which option has been pressed and runs thats activity (class)
        // for this version, position 0 is EmailPasswordActivity
        // and position 1 is GoogleSignInActivity
        Class clicked = CLASSES[position];
        startActivity(new Intent(this, clicked));
    }

    // this is the adapter class used for the list view
    public static class MyArrayAdapter extends ArrayAdapter<Class> {

        // Context is class that holds all information about the activity
        private Context mContext;
        private Class[] mClasses;
        private int[] mDescriptionIds;

        public MyArrayAdapter(Context context, int resource, Class[] objects) {
            super(context, resource, objects);

            mContext = context;
            mClasses = objects;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // View is everything displayed for login screen for each list item
            View view = convertView;

            // the below lines of code is how the customize view is achieved for the list view
            if (convertView == null) {
                // LayoutInflater turns XML code into a view
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.sign_in_list, null);
            }
            // setting the text view for each type of login
            // the view is the same, however the description is different for each login type
            ((TextView) view.findViewById(R.id.EmailandPasswordIcon)).setText(mClasses[position].getSimpleName());
            ((TextView) view.findViewById(R.id.EmailandPasswordIcon)).setText(mDescriptionIds[position]);

            // again setting the image view for each type of login
            // the same id is used for each image, however a different drawable is used for each position
            // The code from line 95 - 101 is the author code
            if (position == 1){
                ((ImageView) view.findViewById(R.id.imageDocIcon)).setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_google));
            }
            else {
                ((ImageView) view.findViewById(R.id.imageDocIcon)).setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_account));

            }
            return view;
        }
        // setting the description text for each type of login
        public void setDescriptionIds(int[] descriptionIds) {
            mDescriptionIds = descriptionIds;
        }

    }


}



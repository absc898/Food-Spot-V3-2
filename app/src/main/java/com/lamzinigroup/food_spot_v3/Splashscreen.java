package com.lamzinigroup.food_spot_v3;


import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.widget.ImageView;


import java.util.Random;

/**
 * Created by apple on 12/07/2017.
 */

// The Splashscreen class has been taken from AndroidMkab:
    // http://androidmkab.com/2016/04/09/how-to-add-animated-splash-screen-to-you-android-app-using-android-studio/
public class Splashscreen extends Activity {


    Thread splashTread;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        imageView = (ImageView)findViewById(R.id.splash);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        int[] ids = new int[]{R.drawable.splash_img,R.drawable.splash_img, R.drawable.splash_img};
        Random randomGenerator = new Random();
        int r= randomGenerator.nextInt(ids.length);
        this.imageView.setImageDrawable(getResources().getDrawable(ids[r]));

        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Splash screen pause time
                    while (waited < 3500) {
                        sleep(100);
                        waited += 100;
                    }
                    Intent intent = new Intent(Splashscreen.this,
                            MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    Splashscreen.this.finish();
                } catch (InterruptedException e) {
                    // do nothing
                } finally {
                    Splashscreen.this.finish();
                }

            }
        };
        splashTread.start();
    }
}

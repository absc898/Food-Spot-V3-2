package com.lamzinigroup.food_spot_v3;

import android.app.ProgressDialog;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by apple on 30/01/2017.
 */


// The BaseActivity Class has been taken from Firebase Database Sample:
    // https://github.com/firebase/quickstart-android/tree/master/database
public class BaseActivity extends AppCompatActivity {

    // This class deals with the Dialog box appearing to the user
    // the methods below are set to public so that other classes can call the dialog box
    @VisibleForTesting
    public ProgressDialog mProgressDialog;

    // this displays a dialog box that says "Please wait..."
    public void showProgressDialog(){
        if (mProgressDialog == null){
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    // This dismisses the dialog box
    public void hideProgressDialog(){
        if (mProgressDialog != null && mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
    }

    @Override
    // When the activity stops (the android application stops) the dialog will go as well
    public void onStop() {
        super.onStop();
        hideProgressDialog();
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }


}

package com.lamzinigroup.food_spot_v3.component;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.lamzinigroup.food_spot_v3.R;

import java.util.List;

/**
 * Created by abdullamzini on 29/05/2017.
 */

// this is a custom view for menu items
    // Lines 24 - 69 are the author code
public class ExpandableList extends LinearLayout {
    private TextView title;
    private ListView listView;

    public ExpandableList(Context context) {
        super(context);
        init(context);
    }

    public ExpandableList(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context){
        inflate(context, R.layout.expandable_layout, this);

        title = (TextView) findViewById(R.id.title);
        listView = (ListView) findViewById(R.id.list_view);

        // this sets a listener for the array, which either hides or displayed the item
        title.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listView.getVisibility() == View.GONE){
                    title.setCompoundDrawablesRelativeWithIntrinsicBounds
                            (0,0,R.drawable.ic_expand_more_black_24dp,0);
                    listView.setVisibility(VISIBLE);
                }
                else {
                    title.setCompoundDrawablesRelativeWithIntrinsicBounds
                            (0,0,R.drawable.ic_expand_less_black_24dp,0);
                    listView.setVisibility(GONE);
                }
            }
        });
    }


    public void setUp(String text, List<String> list){
        // this is where the item title is setup (breakfast, lunch or dinner)
        // this also setups the items within each list
        title.setText(text);
        listView.setAdapter(new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,list));
        setListViewHeightBasedOnChildren(listView);
    }


    // The below function has been taken from Stack Over Flow:
        // https://stackoverflow.com/questions/18367522/android-list-view-inside-a-scroll-view
    // This fix the issue of having a list view within the scroll view
    private void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}

package com.lamzinigroup.food_spot_v3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.lamzinigroup.food_spot_v3.component.ExpandableList;

import java.util.Arrays;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        // the below lines of code is the author code (17) && (19)
        String[] a = {"htht","tt"};

        ((ExpandableList)findViewById(R.id.menu)).setUp("Text", Arrays.asList(a));
    }
}
